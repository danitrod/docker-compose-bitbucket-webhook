#!/usr/bin/bash

if [[ $1 == "main" ]] ; then
        echo "Detected push in main branch. Starting redeploy."
        git pull

        docker-compose down
        docker-compose up -d --build site
fi
