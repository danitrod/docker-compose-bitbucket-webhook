# Docker Compose Bitbucket Webhook

Usage of https://github.com/adnanh/webhook for updating a Laravel environment with Docker Compose on a remote Ubuntu server.

The webhook trigger runs a script that, whenever the main branch is changed, will restart the docker-compose environment.

The Laravel project structure should look something like this: https://github.com/aschmelyun/docker-compose-laravel/

Note: this is probably not suitable for production usage. Whenever the webhook is activated, there is a small downtime for Docker to update the containers.

## Before starting

Edit `hooks.json`'s `command-working-directory` to the path to your local Laravel repository. If you wish to change the default branch, edit the if statement in `redeploy-laravel.sh`.

## Steps to run (assuming a Ubuntu server)

1. `sudo apt-get install webhook`
2. `mkdir /home/ubuntu/webhooks`
3. Copy `hooks.json` to `/home/ubuntu/webhooks`
4. Copy `redeploy-laravel.sh` to `/home/ubuntu/webhooks`
5. Run at `/home/ubuntu/webhooks`: `webhook -hooks hooks.json &`

Done! Your server is now listening for webhooks in the port 9000.
Now configure your Bitbucket repository to send a webhook to `http://<your-server-ip>:9000/hooks/webhook`.
